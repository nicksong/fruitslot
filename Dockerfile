FROM microsoft/aspnetcore:2.0 AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/aspnetcore-build:2.0 AS build
WORKDIR /src
COPY ["FruitSlot/FruitSlot.csproj", "FruitSlot/"]
RUN dotnet restore "FruitSlot/FruitSlot.csproj"
COPY . .
WORKDIR "/src/FruitSlot"
RUN dotnet build "FruitSlot.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "FruitSlot.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "FruitSlot.dll"]