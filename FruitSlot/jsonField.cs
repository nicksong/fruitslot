﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FruitSlot
{
    public class JsonField
    {

        public readonly JObject Json;

        public JsonField(JObject json)
        {
            Json = json;
        }

        public int GetInt(string fieldname,int defaultvalue=0)
        {
            if (Json[fieldname] == null)
                return defaultvalue;

            return int.TryParse(Json[fieldname].ToString(), out int r) ? r : defaultvalue;
        }

        public string GetString(string fieldname)
        {            
             return Json[fieldname]?.ToString();
        }
    }
}
