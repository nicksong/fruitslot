﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace FruitSlot.Db
{
    public class Info
    {
        public readonly AppDb Db;


        public Info(AppDb db)
        {
            Db = db;

        }



        
        private async Task<List<NoticeInfoEntity>> ReadNoticeAsync(DbDataReader reader)
        {
            var posts = new List<NoticeInfoEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new NoticeInfoEntity()
                    {
                        success = await reader.GetFieldValueAsync<int>(0),
                        class1 = await reader.GetFieldValueAsync<int>(1),
                        msg  = await reader.GetFieldValueAsync<string>(2)
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }

        public async Task<NoticeInfoEntity> GetNotice(int userid ,int class1)
        {
            List<NoticeInfoEntity> result = await ReadNoticeAsync(await NoticeCmd(userid, class1).ExecuteReaderAsync());
            return result.FirstOrDefault();
        }

        public async Task<NoticeInfoEntity> Update(int userid, int class1,string msg)
        {
            List<NoticeInfoEntity> result = await ReadNoticeAsync(await UpdateCmd(userid, class1,msg).ExecuteReaderAsync());
            return result.FirstOrDefault();
        }

        private DbCommand UpdateCmd(int userid, int class1,string msg)
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = "updateacct";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@userids",
                DbType = DbType.Int32,
                Value = userid
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@mclass",
                DbType = DbType.Int32,
                Value = class1
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@msg",
                DbType = DbType.String,
                Value = msg
            });
            return cmd as MySqlCommand;
        }
        private DbCommand NoticeCmd(int userid, int class1)
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = "getinfo";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@uid",
                DbType = DbType.Int32,
                Value = userid
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@mclass",
                DbType = DbType.Int32,
                Value = class1
            });

            return cmd as MySqlCommand;
        }

    }
}
