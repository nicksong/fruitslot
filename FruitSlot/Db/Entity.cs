﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FruitSlot.Db
{
    public class Token
    {
        private readonly string _token;

        public string acct
        {
            get
            {
                
                if (string.IsNullOrEmpty(_token))
                    return string.Empty;
                else
                {
                    string[] tokens = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(_token)).Split(',');
                    return tokens[0];
                }
            }
        }

        public string pass
        {
            get
            {
                if (string.IsNullOrEmpty(_token))
                    return string.Empty;
                else
                {
                    string[] tokens = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(_token)).Split(',');
                    return tokens.Length > 1 ? tokens[1] : string.Empty;
                }
            }
        }
        public int? userid
        {
            get
            {
                if (string.IsNullOrEmpty(_token))
                    return null;
                else
                {
                    string[] tokens = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(_token)).Split(',');
                    if (tokens.Length > 2)
                    {                       
                        if (int.TryParse(tokens[2], out int r))
                            return r;
                        else
                            return null;
                    }
                    else
                        return null;
                }
            }
        }
        public Token(string token)
        {

            _token = token;
             
        }

    }

    public class ResponseEntity
    {
        public int idx { get; private set; }
        public int state { get; private set; }
        public int success;

        public void Response(int idx,int state)
        {
            this.idx = idx;
            this.state = state;
        }
    }

    public class VisitorEntity : ResponseEntity
    {
             
        public string acct;
        public string pass;

    }

    public class LoginEntity : ResponseEntity
    {

        public string acct;
        public string pass;
        public string msg;
        public Int64 userid;
        public string username;
        public Int64 socre;
        public Int64 socre2;
        public string tel;
        public Int64 type;
        public string code;
         
        
    }

    public class NoticeInfoEntity : ResponseEntity
    {
        public int class1;        
        public string msg;

    }

}
