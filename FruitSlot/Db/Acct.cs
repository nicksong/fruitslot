﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace FruitSlot.Db
{
    public class Acct
    {
        public readonly AppDb Db;


        public Acct(AppDb db)
        {
            Db = db;

        }
      

        private async Task<List<VisitorEntity>> ReadVisitorAsync(DbDataReader reader)
        {
            var posts = new List<VisitorEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new VisitorEntity()
                    {
                        success = await reader.GetFieldValueAsync<int>(0),
                        acct = await reader.GetFieldValueAsync<string>(1),
                        pass = await reader.GetFieldValueAsync<string>(2)
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
        private async Task<List<LoginEntity>> ReadLoginAsync(DbDataReader reader)
        {
            var posts = new List<LoginEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new LoginEntity()
                    {
                        
                        success =await Db.SafeGetFieldValue(reader, 0, 0),
                        msg = await Db.SafeGetFieldValue(reader, 1, string.Empty),
                        acct = await Db.SafeGetFieldValue(reader, 2, string.Empty),
                        pass = await Db.SafeGetFieldValue(reader, 3, string.Empty),
                        userid =  await Db.SafeGetFieldValue<Int64>(reader, 4, 0),
                        username = await Db.SafeGetFieldValue(reader, 5, string.Empty),
                        socre =  await Db.SafeGetFieldValue<Int64>(reader, 6, 0),
                        socre2 = await Db.SafeGetFieldValue<Int64>(reader, 7, 0),
                        tel = await Db.SafeGetFieldValue(reader, 8, string.Empty),
                        type = await Db.SafeGetFieldValue<Int64>(reader, 9, 0),
                        code = await Db.SafeGetFieldValue(reader, 10, string.Empty)
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }

        private async Task<int?> ReadTokenAsync(DbDataReader reader)
        {
            using (reader)
            {
                if (await reader.ReadAsync())
                {
                    return await reader.GetFieldValueAsync<int>(0);
                }
            }
            return null;
        }


        public async Task<LoginEntity> Login(string acct,string pass)
        {
            List<LoginEntity> result = await ReadLoginAsync(await LoginCmd(acct,pass).ExecuteReaderAsync());
            return result.FirstOrDefault();
        }

        public async Task<int?> ValidToken(string token)
        {
           

            return  await ReadTokenAsync(await ValidTokenCmd(token).ExecuteReaderAsync());
            
        }

        public async Task<VisitorEntity> Visitor()
        {
            List<VisitorEntity> result = await ReadVisitorAsync(await VisitorCmd().ExecuteReaderAsync());
            return result.FirstOrDefault();
        }
        
        private DbCommand ValidTokenCmd(string token)
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = "select userid from accountsinfo t where t.token = @token";



            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@token",
                DbType = DbType.String,
                Value = token
            });
            
            return cmd as MySqlCommand;
        }


        private DbCommand  LoginCmd(string acct, string pass)
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = "logininfo";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@acct",
                DbType = DbType.String,
                Value = acct
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@pass",
                DbType = DbType.String,
                Value = pass
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ip",
                DbType = DbType.String,
                Value = string.Empty
            });
            return cmd as MySqlCommand;
        }

        private DbCommand VisitorCmd()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = "addusers";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@acct",
                DbType = DbType.String,
                Value = string.Empty
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@pass",
                DbType = DbType.String,
                Value = string.Empty
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@tel",
                DbType = DbType.String,
                Value = "yk"
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ip",
                DbType = DbType.String,
                Value = string.Empty
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@yz",
                DbType = DbType.Int32,
                Value = 0
            });
            return cmd as MySqlCommand;
        }
    }
}
