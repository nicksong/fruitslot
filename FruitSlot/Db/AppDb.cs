﻿using MySql.Data.MySqlClient;
using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace FruitSlot.Db
{
    

    public class AppDb : IDisposable
	{

        public async Task<T> SafeGetFieldValue<T>(DbDataReader reader, int idx, T defaultValue)
        {
            return await reader.IsDBNullAsync(idx) ? defaultValue : await reader.GetFieldValueAsync<T>(idx);
        }

        public MySqlConnection Connection;

		public AppDb()
		{
			Connection = new MySqlConnection("server=172.18.161.57;user id=root;password=mztd123456;port=3308;database=shuiguo;maxpoolsize=20;");
		}

		public void Dispose()
		{
			Connection.Close();
		}
	}
}
