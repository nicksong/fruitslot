﻿using FruitSlot.Db;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace FruitSlot
{
    public class ApiKeyAuthOpts : AuthenticationSchemeOptions
    {
    }


    public class ApiKeyAuthHandler : AuthenticationHandler<ApiKeyAuthOpts>
    {

        public ApiKeyAuthHandler(IOptionsMonitor<ApiKeyAuthOpts> options, Microsoft.Extensions.Logging.ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
       : base(options, logger, encoder, clock)
        {
        }

         

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
           
            
            if (!Request.Path.StartsWithSegments("/api", StringComparison.OrdinalIgnoreCase))
            {
               // LogHelper.WriteLog(this.GetType(), Request.Path);
                return AuthenticateResult.NoResult();
            }
            
            string token = Request.Form["code"];

            if (string.IsNullOrEmpty(token))
            {
                return AuthenticateResult.NoResult();
            }

            try
            {
                 
               

                using (var db = new AppDb())
                {
                    await db.Connection.OpenAsync();
                    var query = new Acct(db);

                    int? userid = await query.ValidToken(token);
                    if (userid == null)
                        return AuthenticateResult.NoResult();
                    else
                    {
                         

                        //这里生成一个临时票据，只在当前Request有效
                        var id = new ClaimsIdentity(
                            new Claim[] { new Claim(Startup.ClaimKey, userid.ToString()) },
                         Scheme.Name);

                        ClaimsPrincipal principal = new ClaimsPrincipal(id);
                        var ticket = new AuthenticationTicket(principal, new AuthenticationProperties(), Scheme.Name);
                        return AuthenticateResult.Success(ticket);
                    }
               
                }
            }
            catch
            {

            }
           
            return AuthenticateResult.NoResult();
        }
    }
}
