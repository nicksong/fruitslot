﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FruitSlot.Db;
using System.Collections.Specialized;
using Microsoft.Extensions.Primitives;

using Microsoft.AspNetCore.Authorization;

namespace FruitSlot.Controllers
{
    
    [Produces("application/json")]
    [Route("api/json")]
    [WebApiExceptionFilter]
    [Authorize(AuthenticationSchemes = Startup.DefaultAuthorizedName)]
    public class JsonController : Controller
    {
          

        private int GetUserid()
        {
            var Claim = Request.HttpContext.User.FindFirst(Startup.ClaimKey);
            
            return int.TryParse(Claim.Value,out int r)? r : 0;
            
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("visitor")]  
        public async Task<IActionResult> Visitor([FromForm] int idx,int state)
        {


            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new Acct(db);
                VisitorEntity entity = await query.Visitor();
                entity.Response(idx, state);
                return new OkObjectResult(entity);
            }
             


        }

        [HttpPost]
        [AllowAnonymous]
        [Route("logininfo")]  
        public async Task<IActionResult> Login([FromForm] int idx, int state,string acct,string pass)
        {


            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new Acct(db);
                LoginEntity entity = await query.Login(acct,pass);
                entity.Response(idx, state);
                return new OkObjectResult(entity);
            }



        }

        [HttpPost]
        [Authorize(Policy = Startup.NeedKeyPolicy)]
        [Route("getinfo")]
        public async Task<IActionResult> GetInfo([FromForm] int idx, int state, int class1)
        {
            

            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new Info(db);
                NoticeInfoEntity entity = await query.GetNotice(GetUserid(), class1);
                entity.Response(idx, state);
                return new OkObjectResult(entity);
            }



        }
        [HttpPost]
        [Authorize(Policy = Startup.NeedKeyPolicy)]
        [Route("updateacct")]
        public async Task<IActionResult> UpdateAcct([FromForm] int idx, int state, int class1,string msg)
        { 

          
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new Info(db);
                NoticeInfoEntity entity = await query.Update(GetUserid(), class1,msg);
                entity.Response(idx, state);
                return new OkObjectResult(entity);
            }



        }
        
    }
}